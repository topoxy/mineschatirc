package fr.emse.rootz.mineschat.controls;

import fr.emse.rootz.mineschat.display.components.ColoredTextField;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by vincent on 02/06/15.
 */
public class MCConsole {

    protected ControlsInterface controlsInterface;
    protected String lastMessage = " ";
    protected String actualMessage = " ";
    protected boolean messageAlreadyUp = false;

    public MCConsole () {}

    public void setControlsManager (ControlsInterface controlsInterface) {
        this.controlsInterface = controlsInterface;
    }

    private void sendMessage(String text) {
        lastMessage = text;
        controlsInterface.sendMessage(text);
        messageAlreadyUp = false;
    }

    /**
     * Génère le champ de texte pour la console
     * @return
     */
    public JTextField generateConsole () {
        ColoredTextField textField = new ColoredTextField();
        textField.setFont(DisplaySettings.littleTitleFont);
        textField.addActionListener(e -> {
            if(!textField.getText().trim().isEmpty()) {
                sendMessage(textField.getText().trim());
                textField.setText(" ");
            }
        });
        Action actionUp = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!messageAlreadyUp) {
                    actualMessage = textField.getText();
                    textField.setText(lastMessage);
                    messageAlreadyUp = true;
                }
            }};
        Action actionDown = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(messageAlreadyUp) {
                    textField.setText(actualMessage);
                    messageAlreadyUp = false;
                }
            }};
        textField.getInputMap().put(KeyStroke.getKeyStroke("UP"), "UP");
        textField.getActionMap().put("UP", actionUp);
        textField.getInputMap().put(KeyStroke.getKeyStroke("DOWN"), "DOWN");
        textField.getActionMap().put("DOWN", actionDown);

        return textField;
    }
}
