package fr.emse.rootz.mineschat.interfaces;

import java.awt.*;
import java.awt.image.BufferedImage;

public interface ImageDisplayer {
	void displayImage(BufferedImage image, Dimension area);
	void displayImage(BufferedImage image);
}
