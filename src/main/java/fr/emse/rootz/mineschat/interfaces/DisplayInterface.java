package fr.emse.rootz.mineschat.interfaces;

import fr.emse.rootz.mineschat.display.components.PhasePanel;

import javax.swing.*;

public interface DisplayInterface {
	void changePhase(PhasePanel.Type type);
	void changePhaseDiscretly(PhasePanel.Type type);
	void goPreviousPhase();
	void minimize();
	void maximize();
	void reduce();
	boolean isWindowDeactivated ();
	void signal();
	JFrame getWindow ();
}
