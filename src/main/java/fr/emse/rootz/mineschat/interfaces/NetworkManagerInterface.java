package fr.emse.rootz.mineschat.interfaces;

import fr.emse.rootz.mineschat.data.Person;

import java.util.ArrayList;

public interface NetworkManagerInterface {

	void sendObject(Object object);
	void startDialog();
	void sendDataTransfertInvitation (ArrayList<Person> receivers);
	void startScreenSharing(int idDiscussion);
	void startAudioConference(int selectedDiscussion);
}
