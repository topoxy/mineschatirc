package fr.emse.rootz.mineschat.interfaces;

public interface UserActionListener {
	void discussionButtonPressed();
	void sendFileButtonPressed();
	void screenSharingButtonPressed();
	void audioConferenceButtonPressed();
}
