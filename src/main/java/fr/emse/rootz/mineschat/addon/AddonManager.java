package fr.emse.rootz.mineschat.addon;

import fr.emse.rootz.mineschat.MinesChatSettings;
import fr.emse.rootz.mineschat.controls.ControlsInterface;

/**
 * Created by victor on 03/06/15.
 */
public class AddonManager implements Addon {
    private final static String IDENTIFIER = "lua";
    private final static String AUTHOR = "Mines'Chat";
    private final static String NAME = "Lua base addon";
    private AddonBase addonBase;
    private ControlsInterface controlsInterface;

    public AddonManager(AddonBase addonBase, ControlsInterface controlsInterface) {
        this.addonBase = addonBase;
        this.controlsInterface = controlsInterface;
    }

    @Override
    public String getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getVersion() {
        return MinesChatSettings.MCVersion;
    }

    @Override
    public String getAuthor() {
        return AUTHOR;
    }

    public boolean executeHook(AddonBase.HOOK_TYPE hookType, Object... o) {
        if (hookType == AddonBase.HOOK_TYPE.HOOK_MESSAGESENDING && o.length > 0) {
            String message = (String) o[0];
            if (message.startsWith("/addons")) {
                String words[] = message.split(" ");
                if (words.length > 1) {
                    switch (words[1]) {
                        case "reload":
                            addonBase.initialize();
                            break;
                        case "list":
                            for (Addon addon: addonBase.getAddonList()) {
                                controlsInterface.generateConsole();
                            }
                    }
                }
                return true;
            }
        }
        return false;
    }
}
