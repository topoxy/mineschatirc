package fr.emse.rootz.mineschat.addon.lua;

import fr.emse.rootz.mineschat.MinesChatSettings;
import fr.emse.rootz.mineschat.addon.*;
import fr.emse.rootz.mineschat.controls.ControlsInterface;
import fr.emse.rootz.mineschat.data.DataCollection;
import fr.emse.rootz.mineschat.data.Message;
import fr.emse.rootz.mineschat.display.components.ColoredInfoDialog;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.JsePlatform;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by victor on 02/06/15.
 */
public class LuaBase implements Addon {
    private final static String IDENTIFIER = "lua";
    private final static String AUTHOR = "Mines'Chat";
    private final static String NAME = "Lua base addon";
    private Globals globals;
    private ArrayList<LuaValue> scripts;
    private LuaMinesChatObject luaMinesChatObject;

    public LuaBase(AddonBase addonBase, ControlsInterface controlsInterface) {
        globals = JsePlatform.standardGlobals();
        luaMinesChatObject = new LuaMinesChatObject(controlsInterface);
        scripts = new ArrayList<>();
        globals.set("mineschat", CoerceJavaToLua.coerce(luaMinesChatObject));

        // Chargement des fichiers
        final File folder = new File(DisplaySettings.getUserDataDirectory() + File.separator + "addons" + File.separator);
        if(folder.listFiles() != null) {
            for (File file: folder.listFiles()) {
                if (file.getName().endsWith(".lua")) {
                    addFile(file);
                }
            }
        }
        else
            System.out.println("No lua script detected.");
    }

    @Override
    public String getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getAuthor() {
        return AUTHOR;
    }

    @Override
    public String getVersion() {
        return MinesChatSettings.MCVersion;
    }

    public void addFile(File file) {
        try {
            LuaValue script = globals.loadfile(file.getAbsolutePath());
            if (script != null) {
                script.call();
                scripts.add(script);
                System.out.println("Loaded lua script " + file.getName());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            new ColoredInfoDialog(null, DisplaySettings.optionsImage, "Problème avec un script lua : <br/>" + e.getMessage().replace("\n","<br/>"));
        }
    }

    @Override
    public boolean executeHook(AddonBase.HOOK_TYPE hookType, Object ... o) {
        return luaMinesChatObject.executeHook(hookType, o);
    }

    public class Script implements Addon {
        private LuaValue script;

        public Script() {

        }
        @Override
        public boolean executeHook(AddonBase.HOOK_TYPE hookType, Object... o) {
            return false;
        }

        @Override
        public String getAuthor() {
            return null;
        }

        @Override
        public String getIdentifier() {
            return null;
        }

        @Override
        public String getName() {
            return null;
        }

        @Override
        public String getVersion() {
            return null;
        }
    }
}
