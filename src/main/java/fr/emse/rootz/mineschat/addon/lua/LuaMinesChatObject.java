package fr.emse.rootz.mineschat.addon.lua;

import fr.emse.rootz.mineschat.addon.AddonBase;
import fr.emse.rootz.mineschat.addon.AddonBase.HOOK_TYPE;
import fr.emse.rootz.mineschat.addon.AddonManager;
import fr.emse.rootz.mineschat.controls.ControlsInterface;
import fr.emse.rootz.mineschat.data.DataCollection;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

import java.util.HashSet;
import java.util.Hashtable;

/**
 * Created by victor on 03/06/15.
 */
public class LuaMinesChatObject {
    public static final Class HOOK_TYPE = AddonBase.HOOK_TYPE.class;
    private ControlsInterface controlsInterface;
    private Hashtable<AddonBase.HOOK_TYPE, HashSet<LuaFunction>> hooks;

    public LuaMinesChatObject(ControlsInterface controlsInterface) {
        this.controlsInterface = controlsInterface;
        hooks = new Hashtable<>();
    }

    public LuaValue registerHook(AddonBase.HOOK_TYPE hookType, LuaFunction func) {
        HashSet<LuaFunction> hookList = hooks.get(hookType);
        if (hookList == null) {
            hookList = new HashSet<>();
            hooks.put(hookType, hookList);
        }
        hookList.add(func);
        System.out.println("Hook attached as " + hookType.name() + " : " + func.toString());
        return LuaValue.NIL;
    }

    public LuaValue unregisterHook(AddonBase.HOOK_TYPE hookType, LuaFunction func) {
        HashSet<LuaFunction> hookList = hooks.get(hookType);
        if (hookList != null) {
            hookList.remove(func);
        }
        return LuaValue.NIL;
    }

    public LuaValue sendText(String text) {
        controlsInterface.sendMessage(text);
        return LuaValue.NIL;
    }

    public LuaValue sendText(String text, int idDiscussion) {
        controlsInterface.sendMessage(text, idDiscussion);
        return LuaValue.NIL;
    }

    public LuaValue sendText(String text, int idMessage, int idDiscussion) {
        controlsInterface.sendMessage(text, idMessage, idDiscussion);
        return LuaValue.NIL;
    }

    public LuaValue sendTextAsServer (String text) {
        controlsInterface.sendMessageAsServer(text);
        return LuaValue.NIL;
    }

    public LuaValue sendTextAsServer (String text, int idDiscussion) {
        controlsInterface.sendMessageAsServer(text, idDiscussion);
        return LuaValue.NIL;
    }

    public LuaValue displayText(String message) {
        controlsInterface.displayLocalMessage(message);
        return LuaValue.NIL;
    }

    public LuaValue getNames(int idDiscussion) {
        LuaValue names = new LuaTable();
        for (int idPerson : DataCollection.getDiscussions().get(idDiscussion).getPeople()) {
            String name = DataCollection.getPerson(idPerson).getPseudonyme();
            names.set(name, idPerson);
        }
        return names;
    }

    public boolean executeHook(AddonBase.HOOK_TYPE hookType, Object ... o) {
        HashSet<LuaFunction> hookList = hooks.get(hookType);
        LuaValue retvalue = LuaValue.NIL;
        try {
            if (hookList != null)
                for (LuaFunction luafunction : hookList)
                    switch (o.length) {
                        case 1:
                            retvalue = luafunction.call(CoerceJavaToLua.coerce(o[0]));
                            break;
                        case 2:
                            retvalue = luafunction.call(CoerceJavaToLua.coerce(o[0]), CoerceJavaToLua.coerce(o[1]));
                            break;
                        case 3:
                            retvalue = luafunction.call(CoerceJavaToLua.coerce(o[0]), CoerceJavaToLua.coerce(o[1]), CoerceJavaToLua.coerce(o[2]));
                            break;
                    }
        }
        catch (LuaError error) {
            controlsInterface.displayLocalMessage("<font color=\"red\">" + error.getMessage().replace("\n", "<br/>") + "</font>");
            error.printStackTrace();
        }
        return retvalue.toboolean();
    }

    public LuaValue getNick() {
        return LuaValue.valueOf(DataCollection.getUser().getPseudonyme());
    }
}
