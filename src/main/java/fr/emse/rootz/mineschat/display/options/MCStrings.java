package fr.emse.rootz.mineschat.display.options;

import fr.emse.rootz.mineschat.MinesChatSettings;

import java.util.HashMap;

/**
 * Created by vincent on 02/06/15.
 *
 * Contient le texte des différents boutons et autre.
 */
public class MCStrings {

    private static HashMap<TextType, String> strings = new HashMap<>();

    public static void prepareStrings () {
        strings.put(TextType.TITLE_LABEL, "Mines'Chat");
        strings.put(TextType.SOUND_BUTTON_TOOLTIP, "<html><div style=\"text-align: center;\">Active/désactive la notification sonore</html>");
        strings.put(TextType.MINIMIZE_BUTTON_TOOLTIP, "<html><div style=\"text-align: center;\">Minimise la fenêtre</html>");
        strings.put(TextType.MAXIMIZE_BUTTON_TOOLTIP,
                "<html><div style=\"text-align: center;\">"
                + "Maximise/réduit la fenêtre. Tu peux obtenir le même effet en double-cliquant<br/>"
                + "à un endroit libre de la fenêtre. Si celle-ci est en mode contractée<br/>"
                + "(grâce au bouton juste en dessous), elle s'étirera/rétrécira verticalement."
                + "</html>");
        strings.put(TextType.REDUCE_BUTTON_TOOLTIP,
                "<html><div style=\"text-align: center;\">"
                + "Contracte/rétablit la fenêtre. <br/>"
                + "Fait disparaitre le titre, la liste des connectés et réduit horizontalement la fenêtre.<br/>"
                + "Si la fenêtre est déjà maximisée, ce bouton fait apparaître/disparaître<br/>"
                + "la liste des connectés."
                + "</html>");
        strings.put(TextType.EXIT_BUTTON_TOOLTIP, "<html><div style=\"text-align: center;\">Quitter</html>");
        strings.put(TextType.SERVER_BUTTON_TOOLTIP, "<html><div style=\"text-align: center;\">Permet de créer un serveur sur lequel d'autres <br/>instances de Mines'Chat pourront se connecter."
                + "<br/>Il y en a un par défaut à la ME, donc pas là peine <br/>d'aller ici, à moins tu ne veuilles faire ton propre serveur.</html>");
        strings.put(TextType.CLIENT_BUTTON_TOOLTIP, "<html><div style=\"text-align: center;\">C'est par ici pour se connecter!</html>");
        strings.put(TextType.CLIENT_BUTTON, "Se connecter");
        strings.put(TextType.SERVER_BUTTON, "Serveur");
        strings.put(TextType.SERVER_ADVICE, "(Pour utilisateurs expérimentés)");
        strings.put(TextType.CREDITS_LABEL, "Mines'Chat "+ MinesChatSettings.MCVersion+", \"Créé par des mineurs, pour des mineurs\", Roots EMSE 2014/2015");
        strings.put(TextType.OPTIONS_LABEL, "Options");
        strings.put(TextType.COLOR_LABEL, "Choix du set de couleurs");
        strings.put(TextType.OPTIONS_EXPLICATION_1, "Tout changement est automatiquement sauvegardé.");
        strings.put(TextType.OPTIONS_EXPLICATION_2, "PS : Le changement de couleur nécessite un redémarrage du logiciel.");
        strings.put(TextType.RETURN, "Retour");
        strings.put(TextType.OPTIONS_BUTTON_TOOLTIP, "<html><div style=\"text-align: center;\">Options du logiciel</html>");
        strings.put(TextType.ART_BUTTON_TOOLTIP, "<html><div style=\"text-align: center;\">Montre au monde entier tes fabuleux talents artistiques!</html>");
        strings.put(TextType.DISCUSSION_NAME_DIALOG, "Nom de la discussion");
        strings.put(TextType.DISCUSSION_NAME_DIALOG_EMPTY_WARNING, "Un peu vide ce nom, non ? \n Je sais que tu peux faire mieux!");
        strings.put(TextType.MENU_ITEM_INVITE, "Inviter dans...");
        strings.put(TextType.MENU_ITEM_NEW_DISCUSSION, "Nouvelle discussion");
        strings.put(TextType.MENU_ITEM_QUIT_DISCUSSION, "Quitter la discussion");
        strings.put(TextType.MENU_ITEM_SEND_FILE, "Envoyer un fichier");
        strings.put(TextType.CONNEXION_PHASE_LABEL, "Connexion et Recherche de serveur");
        strings.put(TextType.AUTO_CONNEXION_BUTTON, "Connection automatique");
        strings.put(TextType.AUTO_CONNEXION_LABEL, "Connexion à la ME");
        strings.put(TextType.DISCOVERY_BUTTON_LABEL,
                "<html><div style=\"text-align: center;\">"
                + "Se connecte au serveur de la ME, sinon cherche un serveur local."
                + "</html>");
        strings.put(TextType.MANUAL_CONNEXION_LABEL, "Connexion manuelle");
        strings.put(TextType.MANUAL_CONNEXION_BUTTON, "Se connecter");
        strings.put(TextType.MANUAL_CONNEXION_BUTTON_TOOLTIP,
                "<html><div style=\"text-align: center;\">"
                + "Tente la connexion avec l'adresse IP indiquée à gauche"
                + "</html>");
        strings.put(TextType.SERVER_FOUND, "Serveur trouvé!<br/>");
        strings.put(TextType.CONNECTION_FAILED, "Recherche infructueuse...");
        strings.put(TextType.IP_INVALID, "Cela ne ressemble pas vraiment à une adresse IP...");
        strings.put(TextType.CONNEXION_SUCCEDED, "Connexion réussie!");
        strings.put(TextType.CONNECTING, "Recherche d'un serveur local en cours...");
        strings.put(TextType.SERVER_PHASE_LABEL, "Serveur");
        strings.put(TextType.SERVER_START, "START");
        strings.put(TextType.SERVER_STOP, "STOP");
        strings.put(TextType.SERVER_NAME_CHOICE, "Choix du nom du serveur");
        strings.put(TextType.SERVER_NAME_INVALID, "Nom invalide!");
        strings.put(TextType.IP_ADDRESS_LABEL, "Adresse IP de cette machine : ");
    }

    public static String getString (TextType type) {
        return strings.get(type);
    }

    public enum TextType {  TITLE_LABEL, SOUND_BUTTON_TOOLTIP, MINIMIZE_BUTTON_TOOLTIP, MAXIMIZE_BUTTON_TOOLTIP, REDUCE_BUTTON_TOOLTIP, EXIT_BUTTON_TOOLTIP,
                            SERVER_BUTTON, CLIENT_BUTTON, SERVER_BUTTON_TOOLTIP, CLIENT_BUTTON_TOOLTIP, SERVER_ADVICE, CREDITS_LABEL,
                            OPTIONS_LABEL, COLOR_LABEL, OPTIONS_EXPLICATION_1, OPTIONS_EXPLICATION_2, OPTIONS_BUTTON_TOOLTIP, RETURN,
                            ART_BUTTON_TOOLTIP, DISCUSSION_NAME_DIALOG, DISCUSSION_NAME_DIALOG_EMPTY_WARNING,
                            MENU_ITEM_INVITE, MENU_ITEM_NEW_DISCUSSION, MENU_ITEM_SEND_FILE, MENU_ITEM_QUIT_DISCUSSION,
                            CONNEXION_PHASE_LABEL, AUTO_CONNEXION_LABEL, AUTO_CONNEXION_BUTTON, DISCOVERY_BUTTON_LABEL, MANUAL_CONNEXION_BUTTON, MANUAL_CONNEXION_BUTTON_TOOLTIP,
                            SERVER_FOUND, CONNECTION_FAILED, IP_INVALID, CONNEXION_SUCCEDED, CONNECTING, MANUAL_CONNEXION_LABEL,
                            SERVER_PHASE_LABEL, SERVER_START, SERVER_STOP, SERVER_NAME_CHOICE, SERVER_NAME_INVALID, IP_ADDRESS_LABEL
    }
}
