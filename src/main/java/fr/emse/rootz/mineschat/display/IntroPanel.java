package fr.emse.rootz.mineschat.display;

import fr.emse.rootz.mineschat.display.components.ColoredButton;
import fr.emse.rootz.mineschat.display.components.ColoredLabel;
import fr.emse.rootz.mineschat.display.components.ColoredPanel;
import fr.emse.rootz.mineschat.display.components.PhasePanel;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.display.options.MCStrings;
import fr.emse.rootz.mineschat.interfaces.DisplayInterface;

import javax.swing.*;
import java.awt.*;

import static fr.emse.rootz.mineschat.display.options.MCStrings.getString;

/**
 * Panel d'accueil.
 * Il donne le choix entre se connecter comme Client,
 * ou bien créer un serveur.
 * Il contient également les crédits vers le bas.
 */
public class IntroPanel extends PhasePanel {

	public IntroPanel(DisplayInterface phaseListener, Type type) {
		super(phaseListener, type);
	}

	protected void constructGUI () {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		ColoredPanel centralPanel = new ColoredPanel();
		centralPanel.setLayout(new BoxLayout(centralPanel, BoxLayout.X_AXIS));

		ColoredLabel image = new ColoredLabel(new ImageIcon(DisplaySettings.splashImage));

		ColoredPanel buttonsPanel = new ColoredPanel();
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));

		ColoredButton serverButton = new ColoredButton(getString(MCStrings.TextType.SERVER_BUTTON), DisplaySettings.mineWagonImage, DisplaySettings.secondaryColor);
		//serverButton.setOpaque(false);
		serverButton.setFont(DisplaySettings.computerFont);
		serverButton.setAlignmentX(CENTER_ALIGNMENT);
		serverButton.espacerDuResteAvecBordure(20, 10, Color.white, 5);
		serverButton.addActionListener(e -> phaseListener.changePhase(Type.SERVER));
		serverButton.setToolTipText(getString(MCStrings.TextType.SERVER_BUTTON_TOOLTIP));

		ColoredButton clientButton = new ColoredButton(getString(MCStrings.TextType.CLIENT_BUTTON), DisplaySettings.minerImage, DisplaySettings.secondaryColor);
		clientButton.setFont(DisplaySettings.computerFont);
		clientButton.espacerDuResteAvecBordure(50, 20, Color.white, 6);
		clientButton.setAlignmentX(CENTER_ALIGNMENT);
		clientButton.addActionListener(e -> phaseListener.changePhase(Type.CONNECTION));
		clientButton.setToolTipText(getString(MCStrings.TextType.CLIENT_BUTTON_TOOLTIP));

		ColoredLabel serverAdvice = new ColoredLabel (getString(MCStrings.TextType.SERVER_ADVICE), DisplaySettings.textFont);
		serverAdvice.setBackground(new Color (0, 0, 0, 0));

		buttonsPanel.addGlue(2, 'v');
		buttonsPanel.add(clientButton);
		buttonsPanel.addGlue(1, 'v');
		buttonsPanel.add(serverButton);
		buttonsPanel.add(serverAdvice);
		buttonsPanel.addGlue(2, 'v');

		centralPanel.addGlue(3, 'h');
		centralPanel.add(image);
		centralPanel.addGlue(1, 'h');
        centralPanel.add(buttonsPanel);
		centralPanel.addGlue(4, 'h');
		
		this.add(centralPanel);

		ColoredLabel creditsLabel = new ColoredLabel (getString(MCStrings.TextType.CREDITS_LABEL), DisplaySettings.textFont);
		creditsLabel.setBackground(new Color (0, 0, 0, 0));
		//creditsLabel.setBorder(new EmptyBorder(10, 10, 10, 10));
		this.add(creditsLabel);
		//this.add(new Bla)
	}

	@Override
	public void entering() {
	}

	@Override
	public void exiting() {
	}

}
