package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.data.ArtGrid;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

public class ColoredArtGridPanel extends ColoredPanel implements MouseListener, MouseMotionListener {
	
	private static int sideSize = DisplaySettings.artGridSize*DisplaySettings.artGridCellSize+1;
	private ArtGrid artGrid;
	private boolean paintLine;
	private boolean leftClick = false;
	private boolean rightClick = false;
	
	public ColoredArtGridPanel(ArtGrid artGrid, boolean paintLine) {
		this(paintLine);
		this.artGrid = artGrid;
	}

	public ColoredArtGridPanel(boolean paintLine) {
		super(ColorMode.INVERSED);
		this.setSize(new Dimension(sideSize, sideSize));
		this.setPreferredSize(new Dimension(sideSize, sideSize));
		this.setMaximumSize(new Dimension(sideSize, sideSize));
		this.paintLine = paintLine;
		createEmptyGrid();
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.setAlignmentX(CENTER_ALIGNMENT);
	}
	
	protected static void paintGrid(Graphics2D g, ArtGrid artGrid, boolean paintLine) {
		g.setColor(DisplaySettings.secondaryColor);
		g.fillRect(0,0,sideSize, sideSize);

		g.setColor(DisplaySettings.caseColor);
		for(int i = 0; i < DisplaySettings.artGridSize; i++) {
			for(int j = 0; j < DisplaySettings.artGridSize; j++) {
				if(artGrid.getValue(i, j)) {
					g.fillRect(DisplaySettings.artGridCellSize*i, DisplaySettings.artGridCellSize*j, DisplaySettings.artGridCellSize, DisplaySettings.artGridCellSize);
				}
			}
		}

		if(paintLine) {
			g.setColor(DisplaySettings.lineColor);
			for(int i = 0; i <= DisplaySettings.artGridSize; i++) {
				g.drawLine(DisplaySettings.artGridCellSize*i, 0, DisplaySettings.artGridCellSize*i, sideSize);
			}
			for(int j = 0; j <= DisplaySettings.artGridSize; j++) {
				g.drawLine(0, DisplaySettings.artGridCellSize*j, sideSize, DisplaySettings.artGridCellSize*j);
			}
		}
	}

	public static BufferedImage createImage(ArtGrid artGrid, boolean paintLine) {
	    int w = sideSize;
	    int h = sideSize;
	    BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
	    Graphics2D g = bi.createGraphics();
	    paintGrid(g, artGrid, paintLine);
	    return bi;
	}

	public void createEmptyGrid () {
		artGrid = new ArtGrid();
	}

	protected void paintComponent(Graphics g0) {
		super.paintComponent(g0);
		Graphics2D g = (Graphics2D) g0;

		if(this.artGrid != null)
			paintGrid(g, artGrid, paintLine);
	}

	protected void setValueAtCoordinates (int px, int py, boolean value) {
		int x = px/DisplaySettings.artGridCellSize;
		int y = py/DisplaySettings.artGridCellSize;
		if(x<0)
			x = 0;
		if(x>DisplaySettings.artGridSize-1)
			x = DisplaySettings.artGridSize-1;
		if(y<0)
			y = 0;
		if(y>DisplaySettings.artGridSize-1)
			y = DisplaySettings.artGridSize-1;
		artGrid.setValue(x, y, value);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(leftClick||rightClick) {
			setValueAtCoordinates(e.getX(), e.getY(), leftClick);
			repaint();
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if(leftClick||rightClick) {
			setValueAtCoordinates(e.getX(), e.getY(), leftClick);
			repaint();
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			leftClick = true;
		}
		else if (SwingUtilities.isRightMouseButton(e)) {
			rightClick = true;
		}
		if(leftClick||rightClick) {
			setValueAtCoordinates(e.getX(), e.getY(), leftClick);
			repaint();
		}
		if (SwingUtilities.isLeftMouseButton(e)) {
			leftClick = false;
		}
		else if (SwingUtilities.isRightMouseButton(e)) {
			rightClick = false;
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			leftClick = true;
		}
		else if (SwingUtilities.isRightMouseButton(e)) {
			rightClick = true;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			leftClick = false;
		}
		else if (SwingUtilities.isRightMouseButton(e)) {
			rightClick = false;
		}
	}
	
	public ArtGrid getArtGrid() {
		return artGrid;
	}
}
