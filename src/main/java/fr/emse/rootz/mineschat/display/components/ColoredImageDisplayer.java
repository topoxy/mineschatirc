package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.interfaces.ImageDisplayer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;

public class ColoredImageDisplayer extends JDialog implements ImageDisplayer {

	static Point mouseDownCompCoords;
	protected ColoredPanel panel;
	protected JLabel imageLabel;
	protected ImageIcon imageIcon = null;
	
	public ColoredImageDisplayer(JFrame parent) {
		constructGUI();
		prepareEvents();
		
		this.setUndecorated(true);
		//this.setOpacity(0.98f);
		this.setModal(false);
		this.setSize(550, 700);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(parent);
		this.setAlwaysOnTop(true);
		//this.setVisible(true);
	}
	
	private void constructGUI () {
		panel = new ColoredPanel(ColoredPanel.ColorMode.INVERSED_CLEAR, 10);
		panel.setLayout(new BorderLayout());
		panel.espacerDuReste(10);
		
		imageIcon = new ImageIcon();
		imageLabel = new JLabel();
		panel.add(imageLabel, BorderLayout.CENTER);
		
		this.setContentPane(panel);
	}
	
	private void prepareEvents () {
		addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				mouseDownCompCoords = null;
			}

			public void mousePressed(MouseEvent e) {
				mouseDownCompCoords = e.getPoint();
			}
		});

		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				Point currCoords = e.getLocationOnScreen();
				setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y
						- mouseDownCompCoords.y);
			}
		});
	}
	
	public void displayImage (BufferedImage image, Dimension area) {
		this.setSize(area);
		imageIcon.setImage(image);
		imageLabel.setIcon(imageIcon);
		repaint();
	}
		
	public void displayImage (BufferedImage image) {
		displayImage(image, new Dimension(image.getWidth(), image.getHeight()));
	}

	
}
