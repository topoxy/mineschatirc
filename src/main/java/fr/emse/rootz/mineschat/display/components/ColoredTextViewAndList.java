package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class ColoredTextViewAndList extends ColoredPanel {
	
	public ColoredTextPane textPane;
	public ColoredClientList clientList;
	public int idDiscussion;
	private JScrollPane scrollPane;

	public ColoredTextViewAndList(ColoredTextPane textPane, ColoredClientList clientList, int idDiscussion) {
		super();
		this.textPane = textPane;
		this.clientList = clientList;
		this.idDiscussion = idDiscussion;

		this.setBorder(new LineBorder(DisplaySettings.transparentWhite));
		//this.setBorder(new EmptyBorder(5, 5, 5, 5));

		this.setLayout(new BorderLayout());

		scrollPane = new JScrollPane();
		scrollPane.setViewportView(textPane);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		//scrollPane.setBorder(new LineBorder(DisplaySettings.secondaryColor, 6));
		JScrollBar sb = scrollPane.getVerticalScrollBar();
		sb.setBackground(DisplaySettings.secondaryColor);
		sb.setUI(new ColoredScrollBar());

		this.add(scrollPane, BorderLayout.CENTER);
		this.add(clientList, BorderLayout.EAST);

		if(DisplaySettings.reduced)
			clientList.setVisible(false);
	}

	@Override
	public void updateColors() {
		super.updateColors();
        if(scrollPane != null)
		    scrollPane.getVerticalScrollBar().setBackground(DisplaySettings.secondaryColor);
	}
	
	public void setClientListVisible (boolean bool) {
		clientList.setVisible(bool);
	}
	
	
	public boolean checkForCommandsBefore (String message) {
		return textPane.checkForCommandsBefore(message);
	}
}