package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import java.awt.*;

public class ColoredProgressBar extends ColoredPanel {

	private int sizeX = 400;
	private int sizeY = 50;
	private float pourcentage = 0;
	private Color progressBarBackgroundColor;
	private Color progressBarForegroundColor;
	
	public ColoredProgressBar(int sx, int sy) {
		super(ColorMode.INVERSED);
		this.sizeX = sx;
		this.sizeY = sy;
		this.setSize(new Dimension(sizeX, sizeY));
		this.setPreferredSize(new Dimension(sizeX, sizeY));
		this.setMaximumSize(new Dimension(sizeX, sizeY));
		this.setAlignmentX(CENTER_ALIGNMENT);

		progressBarBackgroundColor = DisplaySettings.getMean(DisplaySettings.secondaryColor, DisplaySettings.primaryColor);
		progressBarForegroundColor = DisplaySettings.getMean(DisplaySettings.primaryColor, Color.white);
	}

	protected void paintComponent(Graphics g0) {
		super.paintComponent(g0);
		Graphics2D g = (Graphics2D) g0;

		g.setColor(progressBarBackgroundColor);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(progressBarForegroundColor);
		g.fillRect(10, 10, (int)((getWidth()-20)*pourcentage/100), getHeight()-20);
	}
	
	public float getPourcentage () {
		return pourcentage;
	}
	
	public void setPourcentage (float p) {
		this.pourcentage = p;
		repaint();
	}
}
