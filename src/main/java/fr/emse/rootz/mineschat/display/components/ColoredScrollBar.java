package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;
import java.awt.*;

public class ColoredScrollBar extends BasicScrollBarUI {
	
	private Color thumbColor = DisplaySettings.getMean(DisplaySettings.primaryColor, DisplaySettings.secondaryColor);
	
	@Override
    protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
		g.setColor(DisplaySettings.primaryColor);
		g.fillRect(trackBounds.x, trackBounds.y, trackBounds.width, trackBounds.height);
    }

    @Override
    protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
		//g.setColor(DisplaySettings.secondaryColor);
		g.setColor(thumbColor);
		g.fillRect(thumbBounds.x, thumbBounds.y, thumbBounds.width, thumbBounds.height);
		g.setColor(DisplaySettings.transparentWhite);
		g.fillRect(thumbBounds.x+5, thumbBounds.y+5, thumbBounds.width-10, thumbBounds.height-10);
    }

}
