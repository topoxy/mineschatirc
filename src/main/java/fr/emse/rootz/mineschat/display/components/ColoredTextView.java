package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.MinesChatSettings;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.ColorChangeable;
import fr.emse.rootz.mineschat.interfaces.TextListener;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.text.DefaultCaret;

public class ColoredTextView extends JTextArea implements TextListener, ColorChangeable {
	
	private JScrollPane scrollPane = null;



	public ColoredTextView() {
		this.setText(" Mines'Chat version : "+MinesChatSettings.MCVersion);
		this.setBackground(DisplaySettings.secondaryColor);
		this.setForeground(DisplaySettings.textColor);
		//this.setBorder(new LineBorder(DisplaySettings.secondaryColor, 6, true));
		this.setFont(DisplaySettings.textFont);
		this.setEditable(false);
		this.setLineWrap(true);
		this.setWrapStyleWord(true);
		DefaultCaret caret = (DefaultCaret)this.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		DisplaySettings.registerColorChangeable(this);
	}

	@Override
	public void writeMessage(String message) {
	   	final String messageToView = message;
		javax.swing.SwingUtilities.invokeLater(() -> {
             append("\n"+"  "+messageToView);
             setCaretPosition(getDocument().getLength());
             if(scrollPane!=null)
                 scrollPane.getVerticalScrollBar().setValue(0);
        });
	}
	
	public JScrollPane rendreScrollable () {
		scrollPane = new JScrollPane(this);
		scrollPane.setBorder(new LineBorder(DisplaySettings.secondaryColor, 6));
		scrollPane.setBackground(DisplaySettings.primaryColor);
		JScrollBar sb = scrollPane.getVerticalScrollBar();
		sb.setBackground(DisplaySettings.secondaryColor);
		sb.setUI(new ColoredScrollBar());
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		return scrollPane;
	}

	@Override
	public void updateColors() {
		scrollPane.setBorder(new LineBorder(DisplaySettings.secondaryColor, 6));
		scrollPane.setBackground(DisplaySettings.primaryColor);
		scrollPane.getVerticalScrollBar().setBackground(DisplaySettings.secondaryColor);
		this.setBackground(DisplaySettings.secondaryColor);
		this.setForeground(DisplaySettings.textColor);
	}
}
