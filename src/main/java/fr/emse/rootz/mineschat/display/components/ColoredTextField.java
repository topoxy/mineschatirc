package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.ColorChangeable;

import javax.swing.*;

/**
 * Created by vincent on 03/06/15.
 */
public class ColoredTextField extends JTextField implements ColorChangeable {

    public ColoredTextField () {
        super(" ");
        DisplaySettings.registerColorChangeable(this);
        updateColors();
    }

    @Override
    public void updateColors() {
        setBackground(DisplaySettings.secondaryColor);
        setForeground(DisplaySettings.textColor);
        setFont(DisplaySettings.littleTitleFont);
        setCaretColor(DisplaySettings.textColor);
    }
}
