package fr.emse.rootz.mineschat.display.options;

import java.awt.*;

public class ColorPair {
	public Color primary;
	public Color secondary;
	
	public ColorPair(Color primary, Color secondary) {
		super();
		this.primary = primary;
		this.secondary = secondary;
	}
	
}