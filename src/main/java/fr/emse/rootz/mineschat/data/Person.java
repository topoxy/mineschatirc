package fr.emse.rootz.mineschat.data;

public class Person implements Identified {
	
	protected int id;
	protected String pseudonyme;
	protected String ip;
	protected int colorIndex;
	
	public Person() {
		this(0, "", "", 0);
	}
	
	public Person(int id, String pseudonyme, String ip, int colorIndex) {
		this.id = id;
		this.pseudonyme = pseudonyme;
		this.ip = ip;
		this.colorIndex = colorIndex;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getPseudonyme() {
		return pseudonyme;
	}

	public void setPseudonyme(String pseudonyme) {
		this.pseudonyme = pseudonyme;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public int getColorIndex() {
		return colorIndex;
	}

	public void setColorIndex(int colorIndex) {
		this.colorIndex = colorIndex;
	}

	/*public Person copy() {
		return new Person(id, pseudonyme, ip, colorIndex);
	}*/

	public Person copy(String version) {
		String limitVersion = "V1.9";
		return new Person(id, pseudonyme, ip, version.compareTo(limitVersion) < 0 ? 0 : colorIndex );
	}
}
