package fr.emse.rootz.mineschat.data;

public class DataArtGrid {

	private ArtGrid artGrid;
	private int idSender;
	private int idDiscussion;
	
	public DataArtGrid() {
		this(null, 0, 0);
	}
	
	public DataArtGrid(ArtGrid artGrid, int idSender, int idDiscussion) {
		super();
		this.artGrid = artGrid;
		this.idSender = idSender;
		this.idDiscussion = idDiscussion;
	}

	public ArtGrid getArtGrid() {
		return artGrid;
	}

	public int getIdSender() {
		return idSender;
	}

	public int getIdDiscussion() {
		return idDiscussion;
	}
	

}
