package fr.emse.rootz.mineschat.data;

public interface Identified {
	int getId();
}
