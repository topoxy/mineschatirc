package fr.emse.rootz.mineschat.data;

import java.util.ArrayList;

public class Discussion implements Identified {
	
	private int id;
	private boolean privateDiscussion;
	private boolean permanent;

	private String name;
	private ArrayList<Integer> people;
	
	public Discussion() {
		this(0,"",null, true, false);
	}
	
	public Discussion(int id, String name, ArrayList<Integer> persons, boolean pri, boolean perm) {
		super();
		this.id = id;
		this.name = name;
		this.people = persons;
		this.privateDiscussion = pri;
		this.permanent = perm;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public ArrayList<Integer> getPeople() {
		return people;
	}

	public boolean isPrivateDiscussion() {
		return privateDiscussion;
	}

	public void setPrivateDiscussion(boolean privateDiscussion) {
		this.privateDiscussion = privateDiscussion;
	}

	public boolean isPermanent() {
		return permanent;
	}

	public void setPermanent(boolean permanent) {
		this.permanent = permanent;
	}
	

}
