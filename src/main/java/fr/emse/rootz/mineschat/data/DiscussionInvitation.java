package fr.emse.rootz.mineschat.data;

import java.util.ArrayList;

public class DiscussionInvitation {
	
	private int idDiscussion;
	private ArrayList<Integer> idInvitedPeople;
	
	public DiscussionInvitation() {
		this(0, null);
	}
	
	public DiscussionInvitation(int idDiscussion, ArrayList<Integer> idInvitedPeople) {
		super();
		this.idDiscussion = idDiscussion;
		this.idInvitedPeople = idInvitedPeople;
	}

	public int getIdDiscussion() {
		return idDiscussion;
	}

	public void setIdDiscussion(int idDiscussion) {
		this.idDiscussion = idDiscussion;
	}

	public ArrayList<Integer> getIdInvitedPeople() {
		return idInvitedPeople;
	}

	public void setIdInvitedPeople(ArrayList<Integer> idInvitedPeople) {
		this.idInvitedPeople = idInvitedPeople;
	}
	
	

}
