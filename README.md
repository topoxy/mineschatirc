# README #

### MinesChat ###

* **Quick summary :** 
MinesChat is a Java communication software primarily intended for the Students' House of the "Mines de Saint-Etienne". It allows chatting and screen-sharing (even over the Internet), and also fast file transfert and audio conference in a local network.

* **Version :**
The last version is the V1.9.

### How do I get set up? ###

* **Summary of set up : **
Just use a decent IDE (i recommend IntelliJ from JetBrains) and import this project from here with the bitbucket link.
* **Configuration**
* **Dependencies : **
You will need at least the JDK 8.
* **How to run tests :** By hand!

### Who do I talk to? ###

* **Repo owner or admin : ** contact me at topoxy314@gmail.com